package com.realdolmen.services;

import com.realdolmen.domain.Soldier;

import java.util.List;

public interface SoldierCommands {
    public void sendToBarracks(Soldier soldier);

    List<Soldier> findAll();
}
