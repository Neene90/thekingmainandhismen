package com.realdolmen.repository;

import com.realdolmen.domain.Soldier;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BarackRepository {
    public void save(Soldier soldier) {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/kingdom", "root", "P@ssw0rd")) {
            PreparedStatement statement = connection.prepareStatement("insert barack(name) values(?)", Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, soldier.getName());
            statement.execute();

            ResultSet generatedKeys = statement.getGeneratedKeys();
            generatedKeys.next();
            soldier.setId(generatedKeys.getInt(1));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Soldier findById(int id) {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/kingdom", "root", "P@ssw0rd")) {
            PreparedStatement statement = connection.prepareStatement("select * from barack where id = ?");
            statement.setInt(1, id);
            statement.execute();

            ResultSet resultSet = statement.getResultSet();
            resultSet.next();
            Soldier soldier = Soldier.builder(resultSet.getString("name")).build();
            return soldier;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }

    }


    public void deleteById(int id) {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/kingdom", "root", "P@ssw0rd")) {
            PreparedStatement statement = connection.prepareStatement("delete from barack where id = ?");
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<Soldier> findAllFromDb() {
        List<Soldier> soldiers = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/kingdom", "root", "P@ssw0rd")) {
            PreparedStatement statement = connection.prepareStatement("select * from barack ");
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next()){
                Soldier soldier = (Soldier) Soldier.builder(resultSet.getString("name"))
                        .id(resultSet.getInt("id"))
                        .build();
                soldiers.add(soldier);
            }
            return soldiers;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return soldiers;
        }
    }
}
